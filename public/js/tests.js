(function() {
	var testMainCSV = 'a,b,c\n1,2,3\n4,5,6\n7,,8\n9,,', 
		testDuplicateCSV = [{album_id:'1',personnel_id:'1'},{album_id:'1',personnel_id:'2'},{album_id:'1',personnel_id:'3'},{album_id:'2',personnel_id:'2'},{album_id:'3',personnel_id:'2'},{album_id:'3',personnel_id:'3'},{album_id:'3',personnel_id:'5'},{album_id:'4',personnel_id:'5'},{album_id:'4',personnel_id:'4'}],
		testMappingCSV = [{d:'1',given_name:'John',surname:'Doe'},{d:'2',given_name:'Ben',surname:'Jamin'},{d:'3',given_name:'Joe',surname:'Shmoe'},{d:'4',given_name:'Pippi',surname:'Long'},{d:'5',given_name:'Max',surname:'Prime'}],
		testDate = '6/7/89',
		passCount = 0,
		retData;

	console.log('Testing parseCSV function');
	
	if (compareObjectArray(parseCSV(testMainCSV), [{a:'1',b:'2',c:'3'},{a:'4',b:'5',c:'6'},{a:'7',b:'—',c:'8'},{a:'9',b:'—',c:'—'}])) {
		passCount++;
		console.log('Passed parseCSV Test');
	}
	else {
		console.log('Failed parseCSV Test');
	}	

	console.log('Testing mergeDuplicateAlbums function');
	
	retData = mergeDuplicateAlbums(testDuplicateCSV);

	if (compareArrayValObject(retData, {1:['1','2','3'],2:['2'],3:['2','3','5'],4:['5','4']})) {
		passCount++;
		console.log('Passed mergeDuplicateAlbums Test');
	}
	else {
		console.log('Failed mergeDuplicateAlbums Test');
	}	

	console.log('Testing createAlbumPersonnelMapping function');

	if (compareStringValObject(createAlbumPersonnelMapping(testMappingCSV, retData), {1:'John Doe, Ben Jamin, Joe Shmoe',2:'Ben Jamin',3:'Ben Jamin, Joe Shmoe, Max Prime',4:'Max Prime, Pippi Long'})) {
		passCount++;
		console.log('Passed createAlbumPersonnelMapping Test');
	}
	else {
		console.log('Failed createAlbumPersonnelMapping Test');
	}			

	console.log('Testing makeSortableDateStr function');

	if (compareString(makeSortableDateStr(testDate), '19890607')) {
		passCount++;
		console.log('Passed makeSortableDateStr Test');
	}
	else {
		console.log('Failed makeSortableDateStr Test');
	}

	console.log('Passed ' + passCount + ' out of 4 Tests');

})();

function compareObjectArray(retData, correctData) {
	for (var i = 0; i < correctData.length; i++) {
		for (var prop in correctData[i]) {
			if (correctData[i][prop] !== retData[i][prop]) {
				return false;
			}
		}
	}

	return true;
}

function compareArrayValObject(retData, correctData) {
	for (var prop in correctData) {
		for (var i = 0; i < correctData[prop].length; i++) {
			if (correctData[prop][i] !== retData[prop][i]) {
				return false;
			}
		}
	}

	return true;
}

function compareStringValObject(retData, correctData) {
	for (var prop in correctData) {
		if (correctData[prop] !== retData[prop]) {
			return false;
		}
	}

	return true;
}

function compareString(retData, correctData) {
	if (correctData !== retData) {
		return false;
	}
	else {
		return true;
	}
}