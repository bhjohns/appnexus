var mainData,
    filters = [],
    personnelData,
    headerMapping = [{propName: 'group', displayName: 'band name'}, {propName: 'title', displayName: 'album title'}, 
                        {propName: 'no_of_discs', displayName: 'number of discs'}, {propName: 'release_date', displayName: 'release date'}, 
                        {propName: 'recording_date', displayName: 'recording date'}, {propName: 'personnel' , displayName: 'personnel'}];

/*
*   Function that will run on startup after dom creation to fetch the CSV data files, parse and manipulate them as needed
*   Then build a table from the data
*/
(function() {
    var dataRequest = new XMLHttpRequest(),
        albumRequest = new XMLHttpRequest(),
        personnelRequest = new XMLHttpRequest(); 

    dataRequest.onreadystatechange=function() {
        if (dataRequest.readyState===4 && dataRequest.status===200) {
            mainData = parseCSV(dataRequest.responseText);

            albumRequest.onreadystatechange=function() {
                if (albumRequest.readyState===4 && albumRequest.status===200) {
                    var albumData = mergeDuplicateAlbums(parseCSV(albumRequest.responseText));

                    personnelRequest.onreadystatechange=function() {
                        if (personnelRequest.readyState===4 && personnelRequest.status===200) {
                            personnelData = createAlbumPersonnelMapping(parseCSV(personnelRequest.responseText), albumData);
                            createBandTable();
                        }
                    }

                    personnelRequest.open("GET","data/personnel.csv",true); 
                    personnelRequest.send();
                }
            }

            albumRequest.open("GET","data/albums_personnel.csv",true); 
            albumRequest.send();
        }
    }

    dataRequest.open("GET","data/albums.csv",true); 
    dataRequest.send();
})();

/*
*   Event handler for clicking any of the check boxes and toggling on and off the visibility of the column
*   In the table representing that data header
*   @param checkbox the checkbox dom element so that we can pull the name from it and get the checked state
*/
function toggleColumnVisible(checkBox) {
    var cells = document.getElementsByName('t' + checkBox.name.replace('check', '')),
        displayMode = checkBox.checked ? 'table-cell' : 'none';

    for (var i = 0; i < cells.length; i++) {
        cells[i].style.display = displayMode;
    }
}

/*
*   Event handler for hitting the enter key in the filter textbox or hitting the add filter button.  Is long as it's not
*   An empty filter value it will add it to our array of filters and rebuild the data table showing only those records that
*   Pass the filter guidelines.
*   @param event the keydown event for when a key is pressed in the text box
*/
function addFilter(event) {
    if (!event || event.keyCode === 13) {
        var filterProp = document.getElementById("filterProp"),
            filterVal = document.getElementById("filterValue"),
            filterList = document.getElementById("filterList"),
            filteredResultsFound = document.getElementById("filteredResultsFound"),
            resultCount,
            html = '';

        if (filterVal.value.length > 0) {
            filters.push({prop: filterProp.value, val: filterVal.value});
            filterVal.value = '';
            html += '<ul>';

            for (var i = 0; i < filters.length; i++) {
                html += '<li>' + filters[i].prop + '=' + filters[i].val + '</li>';
            }

            html += '</ul><input type="button" value="Clear All Filters" onclick="clearFilters()"/>';
            filterList.innerHTML = html;
            resultCount = createBandTable();
            filteredResultsFound.innerHTML = resultCount + ' of ' + mainData.length + ' albums matched your search';
        }
    }
}

/*
*   Event handler for hitting the clear all filters button.  As expected it clears the filters array and rebuilds the data
*   Table with no filters applied
*/
function clearFilters() {
    var filterList = document.getElementById("filterList"),
        filteredResultsFound = document.getElementById("filteredResultsFound");
    filters = [];
    filterList.innerHTML = '';
    filteredResultsFound.innerHTML = '';
    createBandTable();
}

/*
*   A method that checks whether a record passes all the filters in the filter array.  It doesn't check for exact values
*   But rather if the filter value is contained in the data value (this allows us to check for single personnell rather than
*   filtering on all the personnel or on 97 for dates and it will handle cases of 1997 or just 97 in the data)
*   @param index the index in our main data array that the record we are checking is
*   @return bool true or false depending on whether the record passes all the filter tests
*/
function checkRecordPassesFilters(index) {
    var dataVal,
        filterVal;

    for (var i = 0; i < filters.length; i++) {
        if (filters[i].prop === 'personnel') {
            dataVal = personnelData[index + 1].toLowerCase();            
        }
        else {
            dataVal = mainData[index][filters[i].prop].toLowerCase();
        }

        if (!(dataVal.indexOf(filters[i].val.toLowerCase()) >= 0)) {
            return false;
        }
    }

    return true;
}

/*
*   A method that when called will build the data table based on the headerMapping (which detemines the preliminary order
*   of the columns), mainData (our main data array to be rendered in the table), and personnelData (our mapped array of the 
*   album id's to the personnel included)
*   @return recordsDisplayed an int count value of the records that passed the filter tests and being rendered in the table
*/
function createBandTable() {
    var bandTable = document.getElementById('bandTable'),
        tableHTML = '<table class="sortable draggable" id="bandGrid"><tr>',
        recordsDisplayed = 0;

    for (var i = 0; i < headerMapping.length - 1; i++) {
        tableHTML += '<th name="t' + headerMapping[i].propName + '">' + headerMapping[i].displayName + '</th>';
    }

    tableHTML += '<th class="sorttable_nosort" name="t' + headerMapping[headerMapping.length - 1].propName + '">' + headerMapping[headerMapping.length - 1].displayName + '</th>';

    tableHTML += '</tr>';

    for (i = 0; i < mainData.length; i++) {
        if (checkRecordPassesFilters(i)) {
            recordsDisplayed++;
            tableHTML += '<tr>'
        
            for (var j = 0; j < headerMapping.length - 1; j++) {
                tableHTML += '<td name="t' + headerMapping[j].propName + '"';

                if (headerMapping[j].propName === 'release_date' || headerMapping[j].propName === 'recording_date') {
                    var sortableDateStr = makeSortableDateStr(mainData[i][headerMapping[j].propName]);
                    tableHTML += ' sorttable_customkey="' + sortableDateStr + '"';
                }

                tableHTML += '>';

                if (headerMapping[j].propName === 'title') {
                    tableHTML += '<a href="' + mainData[i]['wiki_link'] + '" target="_blank">' + mainData[i][headerMapping[j].propName] +'</a></td>'
                }
                else {
                    tableHTML += mainData[i][headerMapping[j].propName] + '</td>';
                }
            }

            tableHTML+= '<td name="t' + headerMapping[headerMapping.length - 1].propName + '">' + personnelData[mainData[i].id] + '</td>';

            tableHTML += '</tr>'
        }
    }
    
    tableHTML += '</table>';
    bandTable.innerHTML = tableHTML;

    var bandGrid = document.getElementById("bandGrid");
    sorttable.makeSortable(bandGrid);
    dragtable.makeDraggable(bandGrid);
    return recordsDisplayed;
}

/*
*   A helper function that will manipulate a date string to a format that our sorter library knows how to handle and sort
*   @param date a date string
*   @return a date string in the format YYYYMMDD
*/
function makeSortableDateStr(date) {
    if (date === null || date === undefined || date.length === 0 || date === '—') {
        return 0;
    }
    else {
        date = date.match(/(\d{1,2})\/(\d{1,2})\/(\d+)/);
        date.splice(0, 1);
    
        if (date[2].length === 2) {
            var firstDigit = date[2].substr(0, 1);

            if (firstDigit === '0' || firstDigit === '1') {
                date[2] = '20' + date[2];
            }
            else {
                date[2] = '19' + date[2];
            }
        }

        for (var i = 0; i < 2; i++) {
            if (date[i].length === 1) {
                date[i] = '0' + date[i];
            }
        }

        return date[2] + date[0] + date[1];
    }
}

/*
*   A helper function that will process a csv and return an array of objects parsed based on new lines and then 
*   Comma separation and using the first line as the property values for the rest of the data
*   NOTE: some of the values in the data have commas that aren't meant as separators so I had to come up with a way
*   To not split the array at those points as well.  I did so by changing all of those commas into * until after splitting
*   The string and then changing them back after
*   @param csv a csv file in string format
*   @return retData an array of objects parsed from the csv
*/
function parseCSV(csv) {
    var data = csv.split(/\r|\n|\r\n/),
        headers = data[0].split(',');
        retData = [];

    for (var i = 1; i < data.length; i++) {
        var recordStr = data[i].replace(/"([^",]*?),([^",]*?)"/g, '$1*$2'),
            record = recordStr.split(',');
            retObj = {};

        for (var j = 0; j < record.length; j++) {
            record[j] = record[j].replace('*', ',');

            if (record[j] === null || record[j] === undefined || record[j].length === 0) {
                record[j] = '—';
            }
        }

        for (j = 0; j < headers.length; j++) {
            retObj[headers[j]] = record[j];
        }
        
        retData.push(retObj);
    }

    return retData;
}

/*
*   A helper function to trim down the album id to personnel id csv so that each album id will point to
*   An array of personnel id's rather than having a separate entry for each
*   @param albums parsed CSV array object
*   @return retAlbums object where it's a dictionary mapping from the album id to the array of personnel id's
*/
function mergeDuplicateAlbums(albums) {
    var retAlbums = {};

    for (var i = 0; i < albums.length; i++) {
        if (retAlbums[albums[i]['album_id']]) {
            retAlbums[albums[i]['album_id']].push(albums[i]['personnel_id']);
        }
        else {
            retAlbums[albums[i]['album_id']] = [albums[i]['personnel_id']];
        }
    }

    return retAlbums;
}

/*
*   A helper function to map the album id with array of personnel id's to the actual personnel associated with
*   Each id.
*   @param personnel parsed csv array object with the personnel id and name of the personnel associated with the id
*   @param albums object dictionary mapping the album id to the array of personnel id's associated with each
*   @return retMap object mapping from the album id to the list of names of personnel assciated with each
*/
function createAlbumPersonnelMapping(personnel, albums) {
    var retMap = {};

    for (var prop in albums) {
        var personnelIds = albums[prop];
        if (albums.hasOwnProperty(prop)) {
            retMap[prop] = '';

            for (var i = 0; i < personnelIds.length; i++) {
                var fullName = personnel[personnelIds[i] - 1].given_name + ' ' + personnel[personnelIds[i] - 1].surname;

                if (i === 0) {
                    retMap[prop] += fullName;
                }
                else {
                    retMap[prop] += ', ' + fullName;
                }
            }
        }
    }

    return retMap;
}